#!/bin/bash
ZABBIX_DIR='/usr/share/zabbix'

echo -ne 'Install module dbic-report to Zabbix'
cp js/dbic-report.js ${ZABBIX_DIR}/js/
echo -ne 'Include header to dbic-report'
cp include/page_header.php ${ZABBIX_DIR}/include/
echo -ne 'Move page to dbic-report'
cp pages/tr_status.php ${ZABBIX_DIR}/
cp pages/report2.php ${ZABBIX_DIR}/
cp pages/report5.php ${ZABBIX_DIR}/
